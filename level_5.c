#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define BUFSIZE 512

int get_shell()
{
    system("bash");
}

int main(int argc, char * argv[])
{
    unsigned long check = 0;
    char buffer[BUFSIZE];

    if (argc != 2) {
        printf("USAGE: %s <arg>", argv[0]);
    }

    memset(buffer, 0, BUFSIZE);

    strcpy(buffer, argv[1]);
    printf(buffer);

    if (check == 0xdeadbeefdeadbeef) {
        get_shell();
    }
    printf("%8lx\n", check);

    return 0;
}