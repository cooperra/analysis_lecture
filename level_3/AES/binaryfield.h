#ifndef __BINARY_FIELD_H__
#define __BINARY_FIELD_H__

struct divresult
{
    unsigned int quotient;
    unsigned int remainder;
};

struct divresult divide (unsigned int dividend, unsigned int divisor);

unsigned int invert (unsigned int val, unsigned int irr_poly);

unsigned int multiply (unsigned int a, unsigned int b, unsigned int irr_poly);

unsigned int power (unsigned int a, unsigned int b, unsigned int irr_poly);

#endif
