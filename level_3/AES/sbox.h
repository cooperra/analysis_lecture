#ifndef __SBOX_H__
#define __SBOX_H__

unsigned char sbox (unsigned char in);

unsigned char invSbox (unsigned char in);

#endif
