#ifndef __AES_H__
#define __AES_H__

#include "aesalg.h"

void ecb_e (unsigned char * data, int numblocks, unsigned char * key, int keylength);
void ecb_d (unsigned char * data, int numblocks, unsigned char * key, int keylength);

void cbc_e (unsigned char * data, int numblocks, unsigned char * key, int keylength, unsigned char * iv);
void cbc_d (unsigned char * data, int numblocks, unsigned char * key, int keylength, unsigned char * iv);

void ctr (unsigned char * data, int numblocks, unsigned char * key, int keylength, unsigned char * iv);

void inciv (unsigned char * iv);
#endif
