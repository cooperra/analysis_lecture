#include "binaryfield.h"

unsigned int bitlength (unsigned int x)
{
    unsigned int length;
    unsigned int intermediate;

    length = 0;
    intermediate = x;

    while (intermediate != 0) {
        length = length + 1;
        intermediate = intermediate >> 1;
    }
    return length;
}

struct divresult divide (unsigned int dividend, unsigned int divisor)
{
    unsigned int placeholder;
    unsigned int to_subtract;
    unsigned int quotient;
    unsigned int intermediate;
    struct divresult result;
    
    placeholder = 0x80000000;
    quotient = 0;
    to_subtract = divisor;
    intermediate = dividend;

    if (dividend == 0) {
        result.quotient = 0;
        result.remainder = 0;
        return result;
    }
    if (divisor == 0) {
        result.quotient = -1;
        result.remainder = -1;
        return result;
    }

    while (!(placeholder & dividend)) {
        placeholder = placeholder >> 1;
    }
    while (!(placeholder & to_subtract)) {
        to_subtract = to_subtract << 1;
    }

    while (bitlength(divisor) <= bitlength(intermediate)) {
        if (placeholder & intermediate) {
            quotient = quotient ^ placeholder;
            intermediate = intermediate ^ to_subtract;
        }
        placeholder = placeholder >> 1;
        to_subtract = to_subtract >> 1;
    }
    quotient = quotient >> (bitlength(divisor) - 1);

    result.quotient = quotient;
    result.remainder = intermediate;
    return result;
}

unsigned int invert (unsigned int val, unsigned int irr_poly)
{
    unsigned int k;
    unsigned int kk;
    unsigned int ktemp;
    unsigned int dividend;
    unsigned int divisor;
    struct divresult step;

    k = 1;
    kk = 0;
    dividend = irr_poly;
    divisor = val;
    do {
        step = divide (dividend, divisor);
        dividend = divisor;
        divisor = step.remainder;
        ktemp = kk ^ multiply(k, step.quotient, irr_poly);
        kk = k;
        k = ktemp;
    } while (step.remainder != 0);

    return kk;
}

unsigned int multiply (unsigned int a, unsigned int b, unsigned int irr_poly)
{
    unsigned int multiply_result;
    unsigned int placeholder;
    struct divresult reduce;

    multiply_result = a;
    placeholder = 0x80000000;

    if (b == 0) {
        return 0;
    }

    while (!(placeholder & b)) {
        placeholder = placeholder >> 1;
    }

    placeholder = placeholder >> 1;
    while (placeholder > 0) {
        multiply_result = multiply_result << 1;
        if (placeholder & b) {
            multiply_result = multiply_result ^ a;
        }
        placeholder = placeholder >> 1;
    }
    reduce = divide (multiply_result, irr_poly);
    return reduce.remainder;
}

unsigned int power (unsigned int a, unsigned int b, unsigned int irr_poly)
{
    unsigned int pow_result;
    unsigned int placeholder;

    pow_result = a;
    placeholder = 0x80000000;

    if (b == 0) {
        return 1;
    }

    while (!(placeholder & b)) {
        placeholder = placeholder >> 1;
    }

    placeholder = placeholder >> 1;
    while (placeholder > 0) {
        pow_result = multiply(pow_result, pow_result, irr_poly);
        if (placeholder & b) {
            pow_result = multiply (pow_result, a, irr_poly);
        }
        placeholder = placeholder >> 1;
    }

    return pow_result;
}
