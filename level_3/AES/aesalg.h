#ifndef __AESALG_H__
#define __AESALG_H__

#include "keyexpansion.h"
#include "sbox.h"
#include "binaryfield.h"

int encrypt (unsigned char * block, unsigned char * key, int key_len);

int decrypt (unsigned char * block, unsigned char * key, int key_len);

#endif
