#ifndef __PROCESSING_H__
#define __PROCESSING_H__

#define KEY_SIZE 32

int check_math_command(char *buffer, int buffer_size);

int run_math_command(char * buffer, int command_index);

int execute_filesystem_command(char *buffer, int buffer_size);

int run_file_command(char * buffer, int command_index);

int check_exec(char *buffer);

int process_command (char * buffer, int len);

#endif