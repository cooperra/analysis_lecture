#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "AES/processing.h"

#define SERVER_PORT 8888

int main(int argc, char *argv[]) {
    int server_socket, client_socket;
    struct sockaddr_in6 server_address, client_address;
    socklen_t client_address_length;
    char message[1000];
    
    // create socket
    if ((server_socket = socket(AF_INET6, SOCK_STREAM, 0)) == -1) {
        printf("Failed to create socket.\n");
        exit(1);
    }
    
    // configure server address
    memset(&server_address, 0, sizeof(server_address));
    server_address.sin6_family = AF_INET6;
    server_address.sin6_addr = in6addr_any;
    server_address.sin6_port = htons(SERVER_PORT);

    int optval = 1;
    setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
    
    // bind socket to address and port
    if (bind(server_socket, (struct sockaddr *)&server_address, sizeof(server_address)) == -1) {
        printf("Failed to bind socket.\n");
        exit(1);
    }
    
    // listen for incoming connections
    if (listen(server_socket, 5) == -1) {
        printf("Failed to listen for incoming connections.\n");
        exit(1);
    }
    
    printf("Server is listening on port %d.\n", SERVER_PORT);
    
    // accept incoming connections and send/receive messages
    while (1) {
        client_address_length = sizeof(client_address);
        if ((client_socket = accept(server_socket, (struct sockaddr *)&client_address, &client_address_length)) == -1) {
            printf("Failed to accept incoming connection.\n");
            continue;
        }
        
        char client_address_str[INET6_ADDRSTRLEN];
        inet_ntop(AF_INET6, &client_address.sin6_addr, client_address_str, INET6_ADDRSTRLEN);
        printf("Client connected: %s:%d\n", client_address_str, ntohs(client_address.sin6_port));
        
        ssize_t len = -1;
        while (len != 0) {
            len = recv(client_socket, message, sizeof(message), 0);
            int succeed = process_command(message, len);
            printf("HERE\n");
            
            send(client_socket, message, strlen(message), 0);
        }
        
        close(client_socket);
    }
    
    // close server socket
    close(server_socket);
    
    return 0;
}
