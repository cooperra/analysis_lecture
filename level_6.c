#include <stdlib.h>
#include <string.h>

#define BUFSIZE 512

int get_shell()
{
    asm ("push %rax"); // This is needed for some 64 bit nonsense in system
    printf("In get_shell\n");
    system("ls");
    system("bash");
}

int vuln_function() {
    char buffer[BUFSIZE];

    memset(buffer, 0, BUFSIZE);

    gets(buffer);
    printf(buffer);

    return 0;
}

int main()
{
    int junk = 0;
    junk = vuln_function();
    return junk;
}