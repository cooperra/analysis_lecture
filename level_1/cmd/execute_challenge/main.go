// code taken from https://www.linode.com/docs/guides/developing-udp-and-tcp-clients-and-servers-in-go/
package main

import (
	"challenge_1/pkg/networking"
)

func main() {
	networking.RunServer("42424")
}
