package networking

import (
	"bufio"
	"challenge_1/pkg/check"
	"fmt"
	"net"
)

func handle_conn(conn net.Conn) {
	defer conn.Close()
	netData, err := bufio.NewReader(conn).ReadString('\n')
	if err != nil {
		fmt.Println(err)
		return
	}
	if check.CheckAnswer(netData) {
		conn.Write([]byte("Congratulations!\n"))
	} else {
		conn.Write([]byte("Incorrect.  Please try again\n"))
	}
}

func RunServer(port string) {
	PORT := ":" + port
	l, err := net.Listen("tcp", PORT)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer l.Close()

	for {
		c, err := l.Accept()
		if err != nil {
			fmt.Println(err)
			return
		}
		go handle_conn(c)
	}
}
