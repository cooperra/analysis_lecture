all: level_3 level_4 level_5 level_6

level_3: level_3/server.c level_3/AES/aesalg.c level_3/AES/aes.c level_3/AES/binaryfield.c level_3/AES/ctr_challenge.c level_3/AES/keyexpansion.c level_3/AES/processing.c level_3/AES/sbox.c
	gcc -o level_3/level_3 level_3/*.c level_3/AES/*.c

level_4: level_4.c
	gcc -w -fno-stack-protector -O0 -o level_4 level_4.c

level_5: level_5.c
	gcc -w -fno-stack-protector -O0 -o level_5 level_5.c

level_6: level_6.c
	gcc -w -fno-stack-protector -no-pie -O0 -o level_6 level_6.c

clean:
	rm level_3/level_3 level_4 level_5 level_6