import asyncio
import processing
import local_math
import fs


PORT = 22222
PASS_FILE = "super_secret.txt"

async def handle_client(reader, writer):
    data = (await reader.read(255))
    args = processing.get_args(data)
    try:
        result = processing.process_args(args)
        print(result)
        writer.write(result)
        await writer.drain()
    except Exception as e:
        print(e)
        writer.write(b"Error on data sent\n")
        await writer.drain()
    writer.close()

async def main():
    server = await asyncio.start_server(handle_client, '0.0.0.0', PORT)
    async with server:
        await server.serve_forever()

if __name__ == '__main__':
    asyncio.run(main())