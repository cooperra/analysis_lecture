def write(args):
    if len(args) != 2:
        return None
    filename = (b"/tmp/" + args[0].split(b'/')[-1]).decode()
    with open(filename, "wb") as f:
        f.write(args[1])
    return len(args[1]).to_bytes(4, 'big')

def append(args):
    if len(args) != 2:
        return None
    filename = "/tmp/" + args[0].split('/')[-1]
    with open(filename, "a") as f:
        f.write(args[1])
    return len(args[1])