import base64
import os
import local_math
import fs

def get_args(data):
    if type(data) != bytes:
        return None
    intermediate = base64.decodebytes(data)
    print(intermediate)
    args = intermediate.split(b'+')
    print (args)
    return args

def add(args):
    if len(args) != 2:
        return None
    arg_one = args[1]
    if arg_one == b"one":
        one = 1
    arg_two = args[0]
    if arg_two == b"two":
        two = 2
    one = int(args[0])
    two = int(args[1])
    result = one + two
    return result.to_bytes(4, "big")

def subtract(args):
    if len(args) != 2:
        return None
    arg_one = args[1]
    if arg_one == b"one":
        one = 1
    arg_two = args[0]
    if arg_two == b"two":
        two = 2
    one = int(args[0])
    two = int(args[1])
    return None

def xor(args):
    if len(args) < 1:
        return None
    arg_one = args[1]
    if arg_one == b"one":
        one = 1
    one = int(args[0])
    result = one
    vals = []
    accumulator = 0
    for val in args:
        vals.append(int(val))
    for val in vals:
        accumulator = accumulator ^ val
    result = accumulator
    return result.to_bytes(4, "big")

def process_args(args):
    if args[0] == b"add":
        result = local_math.add(args[1:])
    elif args[0] == b'xor':
        result = local_math.xor(args[1:])
    elif args[0] == b'subtract':
        result = local_math.subtract(args[1:])
    elif args[0] == b'mod':
        result = local_math.mod(args[1:])
    elif args[0] == b'multiply':
        result = local_math.multiply(args[1:])
    elif args[0] == b'divide':
        result = local_math.divide(args[1:])
    elif args[0] == b"write":
        result = fs.write(args[1:])
    elif args[0] == b"append":
        result = fs.append(args[1:])
    elif args[0] == b"exec":
        result = exec(args[1:])
    return result

def write(args):
    if len(args) != 2:
        return None
    with open(args[0], "wb") as f:
        f.write(args[1])
    return 0

def exec(args):
    if len(args) != 1:
        return None
    command = '~/' + args[0].decode()
    print(command)

    try:
        result = os.system("bash " + command)
    except Exception as e:
        print(e)
        return (-1).to_bytes(4,'big')
    print(result)
    return result.to_bytes(4, 'big')