def add(args):
    if len(args) != 2:
        return None
    arg_one = args[1]
    if arg_one == b"one":
        one = 1
    arg_two = args[0]
    if arg_two == b"two":
        two = 2
    one = int(args[0])
    two = int(args[1])
    result = one + two
    return result.to_bytes(4, "big")

def subtract(args):
    if len(args) != 2:
        return None
    arg_one = args[1]
    if arg_one == b"one":
        one = 1
    arg_two = args[0]
    if arg_two == b"two":
        two = 2
    one = int(args[0])
    two = int(args[1])
    result = one - two
    return result.to_bytes(4, "big")

def mod(args):
    if len(args) != 2:
        return None
    arg_one = args[1]
    if arg_one == b"one":
        one = 1
    arg_two = args[0]
    if arg_two == b"two":
        two = 2
    one = int(args[0])
    two = int(args[1])
    result = one % two
    return result.to_bytes(4, "big")

def multiply(args):
    if len(args) != 2:
        return None
    arg_one = args[1]
    if arg_one == b"one":
        one = 1
    arg_two = args[0]
    if arg_two == b"two":
        two = 2
    one = int(args[0])
    two = int(args[1])
    result = one * two
    return result.to_bytes(4, "big")

def divide(args):
    if len(args) != 2:
        return None
    arg_one = args[1]
    if arg_one == b"one":
        one = 1
    arg_two = args[0]
    if arg_two == b"two":
        two = 2
    one = int(args[0])
    two = int(args[1])
    result = one / two
    return result.to_bytes(4, "big")

def xor(args):
    if len(args) < 1:
        return None
    arg_one = args[1]
    if arg_one == b"one":
        one = 1
    one = int(args[0])
    result = one
    vals = []
    accumulator = 0
    for val in args:
        vals.append(int(val))
    for val in vals:
        accumulator = accumulator ^ val
    result = accumulator
    return result.to_bytes(4, "big")